package main.java.com.bilgel.yazilim;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestExamApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestExamApiApplication.class, args);
	}

}

