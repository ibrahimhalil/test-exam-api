package com.bilgel.yazilim.service.impl;

import com.bilgel.yazilim.dao.TestExamRepository;
import com.bilgel.yazilim.service.TestExamService;
import main.java.com.bilgel.yazilim.antity.TestExam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.transaction.Transactional;
import java.sql.Date;
import java.util.List;
import java.util.Optional;

public class TestExamServiceImpl implements TestExamService {

    @Autowired
    private TestExamRepository testExamRepository;

    public void setTestExamRepository(@Qualifier("testExamRepository") TestExamRepository testExamRepository) {
        this.testExamRepository = testExamRepository;
    }

    @Transactional
    @Override
    public List<TestExam> findAll() {

        return null;
    }

    @Transactional
    @Override
    public TestExam findById(int theId) {

        Optional<TestExam> result = testExamRepository.findById(theId);
        TestExam tempTestExam = null;

        if (result.isPresent()) {
            tempTestExam = result.get();
        } else {
            // we didn't find the employee
            throw new RuntimeException("Did not find TestExam id - " + theId);
        }

        return tempTestExam;
    }

    @Transactional
    @Override
    public void save(TestExam testExam) {
        testExamRepository.save(testExam);

    }

    @Transactional
    @Override
    public void deleteById(int theId) {
        testExamRepository.deleteById(theId);

    }

    @Transactional
    @Override
    public TestExam findByCreator(String creator) {
        TestExam lists=testExamRepository.findByCreator(creator);
        return lists;
    }



    @Transactional
    @Override
    public List<TestExam> findAllByCreator(String creator) {
        List<TestExam>creatorLists=testExamRepository.findAllByCreator(creator);
        return creatorLists;
    }

    @Transactional
    @Override
    public List<TestExam> listByDateRange(Date startDate, Date endDate) {

        return testExamRepository.listByDateRange(startDate, endDate);
    }



}
