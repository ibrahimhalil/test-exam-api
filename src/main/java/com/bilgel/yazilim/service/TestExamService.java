package com.bilgel.yazilim.service;

import main.java.com.bilgel.yazilim.antity.TestExam;

import java.sql.Date;
import java.util.List;

public interface TestExamService {


        public List<TestExam> findAll();
        public TestExam findById(int theId);
        public void save(TestExam testExam);
        public void deleteById(int theId);
        TestExam findByCreator(String creator);
        List<TestExam> findAllByCreator(String creator);
        public  List<TestExam>listByDateRange(Date startDate, Date endDate);
}
