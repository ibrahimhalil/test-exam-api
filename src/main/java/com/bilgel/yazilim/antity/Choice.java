package com.bilgel.yazilim.antity;

import javax.persistence.*;

@Entity
@Table(name = "choice")
public class Choice {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="choice_id")
    private int id;
    @Column(name="text")
    private String text;
    @Column(name="choice_order")
    private int order;
    @Column(name="is_correct")
    private boolean isCorrect;

    public Choice() {
    }

    public Choice(String text, int order, boolean isCorrect) {
        this.text = text;
        this.order = order;
        this.isCorrect = isCorrect;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public boolean isCorrect() {
        return isCorrect;
    }

    public void setCorrect(boolean isCorrect) {
        this.isCorrect = isCorrect;
    }

    @Override
    public String toString() {
        return "Choice [id=" + id + ", text=" + text + ", order=" + order + ", isCorrect=" + isCorrect + "]";
    }

}
