package com.bilgel.yazilim.antity;


import javafx.scene.input.KeyCode;
import org.hibernate.engine.internal.Cascade;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "question")
public class Question {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "question_id")
    private int id;
    @Column(name = "title")
    private String title;
    @Column(name = "problem_description")
    private String problemDescription;
    @Column(name = "has_multi_answer")
    private boolean hasMultiAnswer;
    @Column(name = "question_order")
    private int order;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "question_id")
    private List<Choice> choices;

    public Question() {
    }

    public Question(String title, String problemDescription, boolean hasMultiAnswer, int order, List<Choice> choices) {

        this.title = title;
        this.problemDescription = problemDescription;
        this.hasMultiAnswer = hasMultiAnswer;
        this.order = order;
        this.choices = choices;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getProblemDescription() {
        return problemDescription;
    }

    public void setProblemDescription(String problemDescription) {
        this.problemDescription = problemDescription;
    }

    public boolean isHasMultiAnswer() {
        return hasMultiAnswer;
    }

    public void setHasMultiAnswer(boolean hasMultiAnswer) {
        this.hasMultiAnswer = hasMultiAnswer;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public List<Choice> getChoices() {
        return choices;
    }

    public void setChoices(List<Choice> choices) {
        this.choices = choices;
    }

    @Override
    public String toString() {
        return "Question [id=" + id + ", title=" + title + ", problemDescription=" + problemDescription
                + ", hasMultiAnswer=" + hasMultiAnswer + ", order=" + order + ", choices=" + choices + "]";
    }
}
