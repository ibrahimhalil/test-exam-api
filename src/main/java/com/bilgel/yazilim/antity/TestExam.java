package main.java.com.bilgel.yazilim.antity;

import com.bilgel.yazilim.antity.Question;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Table(name="testexam")
public class TestExam {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "testexam_id")
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "pass_score")
    private int passScore;
    @Column(name = "creator")
    private String creator;
    @Column(name = "total_score")
    private int totalScore;
    @Column(name = "exam_duration")
    private int examDuration;
    @Column(name="creator_date")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date creatorDate;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "testExam_id")
    private List<Question> questions;

    public TestExam() {
    }

    public TestExam(String name, String description, int passScore, String creator, int totalScore, int examDuration,
                    List<Question> questions,Date creatorDate) {

        this.name = name;
        this.description = description;
        this.passScore = passScore;
        this.creator = creator;
        this.totalScore = totalScore;
        this.examDuration = examDuration;
        this.questions = questions;
        this.creatorDate=creatorDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPassScore() {
        return passScore;
    }

    public void setPassScore(int passScore) {
        this.passScore = passScore;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }

    public int getExamDuration() {
        return examDuration;
    }

    public void setExamDuration(int examDuration) {
        this.examDuration = examDuration;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }



    public Date getCreatorDate() {
        return creatorDate;
    }

    public void setCreatorDate(Date creatorDate) {
        this.creatorDate = creatorDate;
    }

    @Override
    public String toString() {
        return "TestExam [id=" + id + ", name=" + name + ", description=" + description + ", passScore=" + passScore
                + ", creator=" + creator + ", totalScore=" + totalScore + ", examDuration=" + examDuration
                + ", creatorDate=" + creatorDate + ", questions=" + questions + "]";
    }


}

