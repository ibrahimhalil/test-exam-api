package com.bilgel.yazilim.dao.impl;

import com.bilgel.yazilim.dao.TestExamDao;
import main.java.com.bilgel.yazilim.antity.TestExam;

import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.sql.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class TestExamRepositoryImpl implements TestExamDao {

    private static Logger logger = LoggerFactory.getLogger(TestExamRepositoryImpl.class);

    @Autowired
    private EntityManager entityManager;



    @SuppressWarnings("unchecked")
    @Override
    public List<TestExam> listByDateRange(Date startDate, Date endDate) {
        logger.info("listByDateRange start");
        Query theQuery = entityManager.createQuery("from TestExam where creator_date between :startDate and :endDate");
        theQuery.setParameter("startDate", startDate);
        logger.info("startDate "+startDate);
        theQuery.setParameter("endDate", endDate);
        logger.info("endDate "+endDate);
        logger.info("theQuery getResultList "+theQuery.getResultList());
        return theQuery.getResultList();

    }
}
