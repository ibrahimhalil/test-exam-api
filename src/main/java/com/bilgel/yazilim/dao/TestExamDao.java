package com.bilgel.yazilim.dao;
import  main.java.com.bilgel.yazilim.antity.TestExam;

import java.sql.Date;
import java.util.List;

public interface TestExamDao {
    public List<TestExam> listByDateRange(Date startDate, Date endDate);
}
