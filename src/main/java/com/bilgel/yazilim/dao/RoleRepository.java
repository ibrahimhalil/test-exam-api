package com.bilgel.yazilim.dao;

import com.bilgel.yazilim.antity.Role;
import org.springframework.stereotype.Repository;

@Repository("roleRepository")
public interface RoleRepository {
    Role findByRole(String role);
}
