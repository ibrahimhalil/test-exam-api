package com.bilgel.yazilim.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import  main.java.com.bilgel.yazilim.antity.TestExam;

@Repository("testExamRepository")
public interface TestExamRepository extends JpaRepository<TestExam,Integer> ,TestExamDao{

   TestExam findByCreator(String creator);
    List<TestExam> findAllByCreator(String creator);
}
