package com.bilgel.yazilim.rest;

import com.bilgel.yazilim.antity.User;
import com.bilgel.yazilim.service.TestExamService;
import com.bilgel.yazilim.service.impl.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import main.java.com.bilgel.yazilim.antity.TestExam;

import java.sql.Date;
import java.util.List;

@RestController
public class HomeRestController {

    private static Logger logger = LoggerFactory.getLogger(HomeRestController.class);

    private UserService userService;
    private TestExamService testExamService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setTestExamService(TestExamService testExamService) {
        this.testExamService = testExamService;
    }

    @PostMapping("/registration")
    public User registration(@RequestBody User tempUser) {
        User userExists = userService.findUserByUsername(tempUser.getUsername());
        logger.info("registration metod start  --->");
        logger.info("Registered user name ---> : " + userExists);
        if (userExists != null) {
            throw new RuntimeException("There is already a user registered with the username provided  --->");
        }
        logger.info("User with registered information  ---> :" + tempUser);

        return userService.saveUser(tempUser);
    }

    @GetMapping("/creators/{creator}")
    public List<main.java.com.bilgel.yazilim.antity.TestExam> findAllByCreator(@PathVariable String creator) {
        logger.info("findAllByCreator metod start --->");
        List<TestExam> creatorList = testExamService.findAllByCreator(creator);
        logger.info("User should be able to list all exams according to creator  ---> "+creatorList);
        return creatorList;
    }

    @GetMapping("/testExam/{testExamId}")
    public TestExam getTestExamById(@PathVariable int theId) {
        logger.info("getTestExamById metod start --->");
        TestExam tempTestExam = testExamService.findById(theId);
        logger.info("getTestExamById  "+tempTestExam);
        return tempTestExam;
    }

   

    @PostMapping("/testExam")
    public TestExam addTestExam(@RequestBody TestExam tempTestExam) {
        logger.info("AddTestExam metod start");
        tempTestExam.setId(0);
        tempTestExam.setCreatorDate(new Date(System.currentTimeMillis()));
        testExamService.save(tempTestExam);
        logger.info("Registered TextExam ---> "+tempTestExam);
        return tempTestExam;
    }

    @DeleteMapping("/testExam/{testExamId}")
    public String deleteTestExam(@PathVariable int testExamId) {
        logger.info("deleteTestExam  metod start ---> ");
        TestExam tempTestExam = testExamService.findById(testExamId);
        logger.info("Testexam object to delete --->  "+tempTestExam);
        if (tempTestExam == null) {
            throw new RuntimeException("TestExam id not found - " + testExamId);
        }
        testExamService.deleteById(testExamId);
        logger.info("Deleted testexam object  --->  "+tempTestExam);
        logger.info("Deleted TestExam id ---->  "+testExamId);
        return "Deleted TestExam id ----> " + testExamId;
    }

    @GetMapping("/testExam/listbydate")
    public List<TestExam> listByDateRange(@RequestParam("startDate") Date startDate,
                                          @RequestParam("endDate") Date endDate) {
        logger.info("listByDateRange start ");
        List<TestExam> list = testExamService.listByDateRange(startDate, endDate);
        logger.info("listByDateRange returned from the method  --->"+list);
        return list;
    }

    @PutMapping("/testExam")
    public TestExam updateTestExam(@RequestBody TestExam tempTestExam) {
        logger.info("updateTestExam metod start");
        testExamService.save(tempTestExam);
        logger.info("The testexam object has been successfully updated --->"+tempTestExam);
        return tempTestExam;
    }

}
